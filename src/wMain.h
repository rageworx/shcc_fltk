#ifndef __WMAIN_H__
#define __WMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Tooltip.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Text_Editor.H>
#include <FL/Fl_Text_Buffer.H>

#include <string>

#if FL_ABI_VERSION < 10303
    #error "This source need FLTK 1.3.4"
#endif

class wMain
{
    public:
        wMain( int argc, char** argv );
        ~wMain();

    public:
        int Run();
        void SetTitle( const char* t );
        void WidgetCallback( Fl_Widget* w );

    public:
        Fl_Text_Editor* editor() { return edtText; }
        Fl_Text_Buffer* styleBuffer(){ return style_c_cpp_buf; }

    protected:
        void loadconfig();
        void saveconfig();
        void createComponents();

    protected:
        std::string     wintitle;
        std::string     currentPath;

    protected:
        unsigned        langsize;
        int             cfg_winx;
        int             cfg_winy;
        int             cfg_winw;
        int             cfg_winh;
        int             cfg_lidx;
        bool            cfg_loaded;

    protected:
        Fl_Double_Window*   window;
        Fl_Button*          btnCopy;
        Fl_Choice*          chsConvType;
        Fl_Text_Editor*     edtText;
        Fl_Text_Buffer*     bufText;
        Fl_Box*             boxInfo;

    protected:
        Fl_Text_Buffer*     style_c_cpp_buf;

};

#endif /// of __WMAIN_H__
