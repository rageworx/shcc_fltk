#include "wMain.h"
#include "minini.h"
#include "resource.h"

////////////////////////////////////////////////////////////////////////////////

#define DEF_CONFIG_NAME     "shcc_fltk.ini"
#define DEF_WIN_W           300
#define DEF_WIN_H           400

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

typedef struct _langst
{
    const char* clsname;
    const char* dspname;
}langst;

typedef struct _replaceitem
{
    const char  findstr;
    const char* replacestr;
}replaceitem;

const langst Langs[] = {
    { "cpp",        "C,C++,CX,CXX" },
    { "cs",         "C#" },
    { "pascal",     "DELPHI,PASCAL" },
    { "basic",      "BASIC" },
    { "bash",       "BASH, SH" },
    { "html",       "HTML" },
    { "css",        "CSS" },
    { 0, 0 }
};

const replaceitem ReplaceList[] = {
    { '&',      "&amp;" },
    { '<',      "&lt;" },
    { '>',      "&gt;" },
    { '/',      "&frasl;" },
};

const unsigned replaceitemsz = 4;

#ifdef SYNTAXHIGHLIGHT_SUPPORTS
// Some C/C++ styles ...
Fl_Text_Display::Style_Table_Entry style_c_cpp_table[] =
{
     { 0xAAAAAA00, FL_COURIER,         11 }, // A - Plain
     { 0x498B3D00, FL_COURIER_ITALIC,  11 }, // B - Line comments
     { 0x498B3D00, FL_COURIER_ITALIC,  11 }, // C - Block comments
     { 0x3E658200, FL_COURIER,         11 }, // D - Strings
     { 0xBD63C500, FL_COURIER,         11 }, // E - Directives
     { 0xBD63C500, FL_COURIER_BOLD,    11 }, // F - Types
     { 0x569CD600, FL_COURIER_BOLD,    11 }, // G - Keywords
};

const unsigned style_c_cpp_table_sz = 7;

const char* code_c_cpp_keywords[] =
{
    "and",
    "and_eq",
    "asm",
    "bitand",
    "bitor",
    "break",
    "case",
    "catch",
    "compl",
    "continue",
    "default",
    "delete",
    "do",
    "else",
    "false",
    "for",
    "goto",
    "if",
    "new",
    "not",
    "not_eq",
    "operator",
    "or",
    "or_eq",
    "return",
    "switch",
    "template",
    "this",
    "throw",
    "true",
    "try",
    "while",
    "xor",
    "xor_eq"
};

const char* code_c_cpp_types[] =
{
    "auto",
    "bool",
    "char",
    "class",
    "const",
    "const_cast",
    "double",
    "dynamic_cast",
    "enum",
    "explicit",
    "extern",
    "float",
    "friend",
    "inline",
    "int",
    "long",
    "mutable",
    "namespace",
    "private",
    "protected",
    "public",
    "register",
    "short",
    "signed",
    "sizeof",
    "static",
    "static_cast",
    "struct",
    "template",
    "typedef",
    "typename",
    "union",
    "unsigned",
    "virtual",
    "void",
    "volatile",
    "uint8",
    "uint16",
    "uint32",
    "uint64",
    "int8",
    "int16",
    "int32",
    "int64",
    "BYTE",
    "WORD",
    "DWORD",
    "HANDLE",
    "HINSTANCE"
};
#endif /// of SYNTAXHIGHLIGHT_SUPPORTS

////////////////////////////////////////////////////////////////////////////////

void fl_w_cb( Fl_Widget* w, void* p );
void te_changed_cb(int, int nInserted, int nDeleted,int, const char*, void* v);

#ifdef SYNTAXHIGHLIGHT_SUPPORTS
extern "C" {
int  compare_keywords(const void *a, const void *b);
}

void style_parse(const char* text, char* style, int length );
void style_init( Fl_Text_Buffer* &stylebuf, Fl_Text_Buffer* textbuf );
void style_update( int pos, int nInserted, int nDeleted, int nRestyled,
                   const char* deletedText, void* cbArg );
void style_unfinished_cb(int a, void* b);
#endif /// of SYNTAXHIGHLIGHT_SUPPORTS

////////////////////////////////////////////////////////////////////////////////

wMain::wMain( int argc, char** argv )
 : langsize( 0 ),
   cfg_winx( 0 ),
   cfg_winy( 0 ),
   cfg_winw( DEF_WIN_W ),
   cfg_winh( DEF_WIN_H ),
   cfg_lidx( 0 ),
   cfg_loaded( false ),
   window( NULL ),
   chsConvType( NULL ),
   edtText( NULL ),
   bufText( NULL ),
   boxInfo( NULL ),
   style_c_cpp_buf( NULL )
{
    string argvextractor = argv[0];

#ifdef DEBUG
    char splitter[] = "/";
#else
    char splitter[] = "\\";
#endif

    if ( argvextractor.size() > 0 )
    {
        string::size_type lastSplitPos = argvextractor.rfind( splitter );

        currentPath = argvextractor.substr(0, lastSplitPos + 1 );
    }

    loadconfig();
    createComponents();

    if ( cfg_loaded == true )
    {
        window->resize( cfg_winx, cfg_winy, cfg_winw, cfg_winh );
        if( cfg_lidx < chsConvType->size() )
        {
            chsConvType->value( cfg_lidx );
        }
    }
}

wMain::~wMain()
{
    saveconfig();
}

void wMain::loadconfig()
{
    string cfile  = currentPath;
           cfile += DEF_CONFIG_NAME;

    if ( access( cfile.c_str(), F_OK ) != 0 )
    {
        return;
    }

    minIni* ini = new minIni( cfile );
    if ( ini != NULL )
    {
        cfg_winx = ini->geti( "POSITION", "X", 10 );
        cfg_winy = ini->geti( "POSITION", "Y", 50 );
        cfg_winw = ini->geti( "POSITION", "W", DEF_WIN_W );
        cfg_winh = ini->geti( "POSITION", "H", DEF_WIN_H );
        cfg_lidx = ini->geti( "OPTION", "LIDX", 0 );

        delete ini;

        cfg_loaded = true;
    }
}

void wMain::saveconfig()
{
    string cfile  = currentPath;
           cfile += DEF_CONFIG_NAME;

    if ( access( cfile.c_str(), F_OK ) != 0 )
    {
        return;
    }

    minIni* ini = new minIni( cfile );
    if ( ini != NULL )
    {
        ini->put( "POSITION", "X", window->x() );
        ini->put( "POSITION", "Y", window->y() );
        ini->put( "POSITION", "W", window->w() );
        ini->put( "POSITION", "H", window->h() );
        ini->put( "OPTION", "LIDX", chsConvType->value() );

        delete ini;
    }
}

int wMain::Run()
{
    if ( window != NULL )
    {
        return Fl::run();
    }

    return 0;
}

void wMain::SetTitle( const char* t )
{
    wintitle = t;
    window->label( wintitle.c_str() );
}

void wMain::WidgetCallback( Fl_Widget* w )
{
    if ( w != NULL )
    {
        if ( w == btnCopy )
        {
            if ( edtText == NULL )
                return;

            if ( bufText == NULL )
                return;

            if ( bufText->length() == 0 )
                return;

            // check first buffer starts ... <pre>
            if ( strncmp( bufText->text(), "<pre", 4 ) == 0 )
            {
                if ( boxInfo != NULL )
                {
                    boxInfo->label( "CANCELLED: Seems to already converted." );
                }
                return;
            }

            unsigned refsz  = bufText->length();

            if ( refsz > 5 )
            {
                string srcstr = bufText->text();

                unsigned findque = 0;

                while( true )
                {
                    unsigned findquemax = srcstr.size();

                    if ( findque >= findquemax )
                        break;

                    unsigned findqueadd = 1;

                    // Replace tistroy-safe ...
                    for( unsigned cnt=0; cnt<replaceitemsz; cnt++ )
                    {
                        // reset buffer pointer ...
                        if ( srcstr[findque] == ReplaceList[cnt].findstr )
                        {
                            srcstr.replace( findque,
                                            1,
                                            ReplaceList[ cnt ].replacestr );

                            findque += strlen( ReplaceList[ cnt ].replacestr );
                            findqueadd = 0;
                        }
                    }

                    findque += findqueadd;
                }

                bufText->text( srcstr.c_str() );

                char insstr[128] = {0};

                sprintf( insstr, "<pre><code class=\"" );

                int idx = chsConvType->value();

                if ( ( idx >= 0 ) && ( idx < langsize ) )
                {
                    const char* refs = Langs[ idx ].clsname;
                    if ( refs != NULL )
                    {
                        strcat( insstr, refs );
                    }
                }

                strcat( insstr, "\">");

                bufText->insert( 0, insstr );
                bufText->insert( bufText->length(), "\n</code></pre>\n");

            }

            // Copy all to clipboard !
            Fl::copy( bufText->text(), bufText->length(), 1,  Fl::clipboard_plain_text );

            if ( boxInfo != NULL )
            {
                boxInfo->label( "CONVERTED & COPIED TO CLIPBOARD: use paste to TiStory editor." );
            }
        }
    }
}

void wMain::createComponents()
{
    wintitle  = "SHCC_FLTK for web-safe";
    wintitle += " v.";
    wintitle += APP_VERSION_STR;

    int win_w = DEF_WIN_W;
    int win_h = DEF_WIN_H;
    int cur_x = 5;
    int cur_y = 5;
    int cur_w = 0;
    int cur_h = 0;

    window = new Fl_Double_Window( win_w, win_h, wintitle.c_str() );
    if ( window != NULL )
    {
        window->labelfont( FL_FREE_FONT );
        window->labelsize( 11 );
        window->labelcolor( FL_WHITE );
        window->color( FL_DARK3 );

        window->begin();

        cur_w = 80;

        Fl_Group* gpDiv0 = new Fl_Group( 0,0,win_w,30);
        if ( gpDiv0 != NULL )
        {
            gpDiv0->begin();
        }

        Fl_Group* gpDiv01 = new Fl_Group( cur_x, cur_y, cur_w, 20 );
        if ( gpDiv01 != NULL )
        {
            gpDiv01->begin();
        }

        btnCopy = new Fl_Button( cur_x, cur_y, cur_w, 20 , "&Convert" );
        if ( btnCopy != NULL )
        {
            Fl_Color bgcol = fl_darker( FL_DARK3 );

            btnCopy->box( FL_FLAT_BOX );
            btnCopy->color( bgcol, fl_darker( bgcol ) );
            btnCopy->labelfont( FL_FREE_FONT );
            btnCopy->labelsize( 11 );
            btnCopy->labelcolor( FL_WHITE );
            btnCopy->tooltip( "Convert codes and copy them to clipboard" );
            btnCopy->clear_visible_focus();
            btnCopy->callback( fl_w_cb, this );
        }

        if ( gpDiv01 != NULL )
        {
            gpDiv01->end();
        }

        // For seperator ..
        Fl_Group* gpDiv02 = new Fl_Group( cur_x + cur_w , cur_y,
                                          win_w - 180 - cur_x, 20 );
        if ( gpDiv02 != NULL )
        {
            gpDiv02->end();
        }

        cur_w = win_w - 180;

        Fl_Group* gpDiv03 = new Fl_Group( win_w - cur_w - cur_x, cur_y,
                                          cur_w , 20 );
        if ( gpDiv03 != NULL )
        {
            gpDiv03->begin();
        }

        chsConvType = new Fl_Choice( win_w - cur_w - cur_x, cur_y,
                                     cur_w, 20,
                                     "Language:" );
        if ( chsConvType != NULL )
        {
            chsConvType->box( FL_THIN_UP_BOX );
            chsConvType->down_box( FL_FLAT_BOX );
            chsConvType->color( FL_DARK3 );
            chsConvType->labelcolor( FL_WHITE );
            chsConvType->labelfont( FL_FREE_FONT );
            chsConvType->labelsize( 11 );
            chsConvType->textcolor( 0xFF990000 );
            chsConvType->textfont( FL_FREE_FONT );
            chsConvType->textsize( 11 );
            chsConvType->tooltip( "Select your code brush type." );
            chsConvType->clear_visible_focus();

            unsigned cnt = 0;
            while( true )
            {
                if ( Langs[cnt].dspname != NULL )
                {
                    chsConvType->add( Langs[cnt].dspname );
                    cnt++;
                }
                else
                {
                    break;
                }
            }

            langsize = cnt;

            chsConvType->value( 0 );
        }

        if ( gpDiv03 != NULL )
        {
            gpDiv03->end();
        }

        if ( gpDiv0 != NULL )
        {
            gpDiv0->end();

            if ( gpDiv02 != NULL )
            {
                gpDiv0->resizable( gpDiv02 );
            }
        }

        cur_x = 0;
        cur_y += 20 + 5;
        cur_w = win_w;
        cur_h = win_h - cur_y - 20;

        Fl_Group* gpDiv1 = new Fl_Group( cur_x, cur_y, cur_w, cur_h );
        if ( gpDiv1 != NULL )
        {
            gpDiv1->begin();
        }

        edtText = new Fl_Text_Editor( cur_x, cur_y, cur_w, cur_h );
        if ( edtText != NULL )
        {
            Fl_Color bgcol = fl_darker( FL_DARK3 );

            edtText->box( FL_FLAT_BOX );
            edtText->color( bgcol, bgcol );
            edtText->color2( bgcol );
            edtText->textcolor( FL_WHITE );
            edtText->textfont( FL_COURIER );
            edtText->textsize( 11 );
            edtText->linenumber_font( FL_COURIER );
            edtText->linenumber_size( 9 );
            edtText->linenumber_width( 30 );
            edtText->linenumber_format( "%05d" );
            edtText->linenumber_bgcolor( fl_darker( bgcol ) );
            edtText->linenumber_fgcolor( FL_GRAY );
            edtText->cursor_color( 0xFF990000 );
            edtText->selection_color( 0xAA770000 );

            edtText->scrollbar_color( bgcol, fl_darker( bgcol ) );
            edtText->scrollbar_width( 10 );

            bufText = new Fl_Text_Buffer();
            if ( bufText != NULL )
            {
#ifdef SYNTAXHIGHLIGHT_SUPPORTS
                style_init( style_c_cpp_buf, bufText );
                edtText->buffer( bufText );

                edtText->highlight_data( style_c_cpp_buf,
                                         style_c_cpp_table,
                                         style_c_cpp_table_sz - 1,
                                         'A',
                                         style_unfinished_cb,
                                         0 );

                bufText->add_modify_callback( style_update, this );
                bufText->add_modify_callback( te_changed_cb, this );
#else
                edtText->buffer( bufText );
                bufText->add_modify_callback( te_changed_cb, this );
#endif /// of SYNTAXHIGHLIGHT_SUPPORTS
            }


        }

        if ( gpDiv1 != NULL )
        {
            gpDiv1->end();
        }

        cur_y += cur_h;

        Fl_Group* gpDiv2 = new Fl_Group( 0, cur_y, win_w, 25 );

        if ( gpDiv2 != NULL )
        {
            gpDiv2->begin();
        }

        cur_h  = 20;

        boxInfo = new Fl_Box( 0, cur_y, win_w, cur_h, "(C)2017, Raph.K. (rageworx@@gmail.com)" );
        if ( boxInfo != NULL )
        {
            Fl_Color bgcol = fl_darker( fl_darker( FL_DARK3 ) );

            boxInfo->align( FL_ALIGN_INSIDE | FL_ALIGN_LEFT | FL_ALIGN_CLIP );
            boxInfo->box( FL_FLAT_BOX );
            boxInfo->color( FL_DARK3 );
            boxInfo->labelfont( FL_FREE_FONT );
            boxInfo->labelsize( 11 );
            boxInfo->labelcolor( FL_WHITE );
        }

        if ( gpDiv2 != NULL )
        {
            gpDiv2->end();
        }

        window->end();

        if ( gpDiv1 != NULL )
        {
            window->resizable( gpDiv1 );
        }

        window->icon((char*)LoadIcon(fl_display, MAKEINTRESOURCE( IDC_ICON_A ) ) );
        window->size_range( win_w, win_h );
        window->show();

        if ( bufText != NULL )
        {
            bufText->call_modify_callbacks();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

void fl_w_cb( Fl_Widget* w, void* p )
{
    if ( p != NULL )
    {
        wMain* win = (wMain*)p;
        win->WidgetCallback( w );
    }

}

void te_changed_cb(int pos, int nInserted, int nDeleted,int, const char* d, void* v)
{
#ifdef SYNTAXHIGHLIGHT_SUPPORTS
    wMain *w = (wMain*)v;

    if ( w != NULL )
    {
        Fl_Text_Editor* e = w->editor();
        if ( e != NULL )
        {
            e->show_insert_position();
        }
    }
#endif /// of SYNTAXHIGHLIGHT_SUPPORTS
}

#ifdef SYNTAXHIGHLIGHT_SUPPORTS
extern "C" int compare_keywords(const void *a, const void *b)
{
    return (strcmp(*((const char **)a), *((const char **)b)));
}

// ---------------------------------------------------------------
// reference : FLTK test editor.cxx.
void style_parse( const char* text, char* style, int length )
{
    char    current;
    int     col;
    int     last;
    char    buf[255] = {0};
    char*   bufptr = NULL;

    const char* temp            = NULL;
    const char** code_keywords  = code_c_cpp_keywords;
    const char** code_types     = code_c_cpp_types;


    // Style letters:
    //
    // A - Plain
    // B - Line comments
    // C - Block comments
    // D - Strings
    // E - Directives
    // F - Types
    // G - Keywords

    for ( current = *style, col = 0, last = 0; length > 0; length --, text ++ )
    {
        if ( ( current == 'B' ) || ( current == 'F' ) || ( current == 'G' ) )
            current = 'A';

        if (current == 'A')
        {
            // Check for directives, comments, strings, and keywords...
            if (col == 0 && *text == '#')
            {
                // Set style to directive
                current = 'E';
            }
            else
            if ( strncmp(text, "//", 2) == 0 )
            {
                current = 'B';

                for (; length > 0 && *text != '\n'; length --, text ++)
                {
                    *style++ = 'B';
                }

                if (length == 0)
                    break;

            }
            else
            if (strncmp(text, "/*", 2) == 0)
            {
                current = 'C';
            }
            else
            if (strncmp(text, "\\\"", 2) == 0)
            {
                // Quoted quote...
                *style++ = current;
                *style++ = current;
                text ++;
                length --;
                col += 2;
                continue;
            }
            else
            if (*text == '\\')
            {
                current = 'D';
            }
            else
            if ( (*text == '\"') || (*text == '\'') )
            {
                current = 'D';
            }
            else
            if ( !last && (islower((*text)&255) || *text == '_') )
            {
                // Might be a keyword...
                for ( temp = text, bufptr = buf;
                      (islower((*temp)&255) || *temp == '_') && bufptr < (buf + sizeof(buf) - 1);
                      *bufptr++ = *temp++)
                {
                    // nothing, just counting .
                }

                if ( !islower((*temp)&255) && *temp != '_' )
                {
                    *bufptr = '\0';

                    bufptr = buf;

                    if ( bsearch(&bufptr, code_types,
                         sizeof(code_types) / sizeof(code_types[0]),
                         sizeof(code_types[0]), compare_keywords))
                    {
                        while (text < temp)
                        {
                            *style++ = 'F';
                            text ++;
                            length --;
                            col ++;
                        }

                        text --;
                        length ++;
                        last = 1;
                        continue;
                    }
                    else
                    if ( bsearch( &bufptr, code_keywords,
                                  sizeof(code_keywords) / sizeof(code_keywords[0]),
                                  sizeof(code_keywords[0]), compare_keywords))
                    {
                        while (text < temp)
                        {
                            *style++ = 'G';
                            text ++;
                            length --;
                            col ++;
                        }

                        text --;
                        length ++;
                        last = 1;
                        continue;
                    }
                }
            }

        }
        else
        if ( current == 'C' )
        {
            if ( strncmp(text, "*/", 2) == 0)
            {
                // Close a C comment...
                *style++ = current;
                *style++ = current;
                text ++;
                length --;
                current = 'A';
                col += 2;
                continue;
            }
        }
        else
        if (current == 'D')
        {
            char dbgstr[32] = {0};
            int  cpsz = min( length, 10 );
            memcpy( dbgstr, text, cpsz );

            if ( ( strncmp( text, "\\\"", 2 ) == 0 ) ||
                 ( strncmp( text, "\\\'", 2 ) == 0 ) )
            {
                // Quoted end quote...
                *style++ = current;
                *style++ = current;
                text ++;
                length --;
                col += 2;
                current = 'A';
                continue;
            }
            else
            if ( ( strncmp( text, "\"\"", 2 ) == 0 ) ||
                 ( strncmp( text, "\'\'", 2 ) == 0 ) ||
                 ( strncmp( text, "\'\"", 2 ) == 0 ) ||
                 ( strncmp( text, "\"\'", 2 ) == 0 ) )
            {
                // Quoted end quote...
                printf("CASE2 : %d => [%s]\n", col, dbgstr );
                *style++ = current;
                *style++ = current;
                text ++;
                length --;
                col += 2;
                current = 'A';
                continue;
            }
            else
            if ( ( *text == '\"' ) || ( *text == '\'' ) )
            {
                *style++ = current;
                col ++;
                current = 'A';
                continue;
            }
        }

        // Copy style info...
        if ( current == 'A' )
        {
            if( ( *text == '{' ) || ( *text == '}') )
            {
                *style++ = 'G';
            }
        }
        else
            *style++ = current;

        col ++;

        last = isalnum((*text)&255) || *text == '_' || *text == '.';

        if (*text == '\n')
        {
          // Reset column and possibly reset the style
          col = 0;

          if (current == 'B' || current == 'E')
            current = 'A';
        }
    }
}

void style_init( Fl_Text_Buffer* &stylebuf, Fl_Text_Buffer* textbuf )
{
    char *style = new char[ textbuf->length() + 1 ];

    if ( style == NULL )
        return;

    const char *text = textbuf->text();

    memset(style, 'A', textbuf->length());
    style[textbuf->length()] = '\0';

    if ( stylebuf == NULL )
    {
        stylebuf = new Fl_Text_Buffer( textbuf->length() );

        if ( stylebuf == NULL )
            return;
    }

    style_parse( text, style, textbuf->length() );

    stylebuf->text(style);

    //delete[] style;
    //free(text);
}

void style_update( int pos, int nInserted, int nDeleted, int nRestyled,
                   const char* deletedText, void * cbArg )
{
    if ( cbArg == NULL )
        return;

    int	    start;
    int     end;
    char    last;
    char*   style = NULL;
    char*   text = NULL;

    wMain*           wm       = (wMain*)cbArg;
    Fl_Text_Editor*  editor   = wm->editor();
    Fl_Text_Buffer*  textbuf  = editor->buffer();
    Fl_Text_Buffer*  stylebuf = wm->styleBuffer();

    if (nInserted == 0 && nDeleted == 0)
    {
        stylebuf->unselect();
        return;
    }

    // Track changes in the text buffer...
    if (nInserted > 0)
    {
        // Insert characters into the style buffer...
        style = new char[nInserted + 1];

        if ( style == NULL )
            return;

        memset(style, 'A', nInserted);
        style[nInserted] = '\0';

        stylebuf->replace(pos, pos + nDeleted, style);

        delete[] style;
    }
    else
    {
        stylebuf->remove(pos, pos + nDeleted);
    }

    stylebuf->select(pos, pos + nInserted - nDeleted);

    start = textbuf->line_start(pos);
    end   = textbuf->line_end(pos + nInserted);
    text  = textbuf->text_range(start, end);
    style = stylebuf->text_range(start, end);

    if ( start == end )
        last = 0;
    else
        last  = style[end - start - 1];

    style_parse(text, style, end - start);
    stylebuf->replace(start, end, style);

    editor->redisplay_range(start, end);

    if (start==end || last != style[end - start - 1])
    {
        free(text);
        free(style);

        end   = textbuf->length();
        text  = textbuf->text_range(start, end);
        style = stylebuf->text_range(start, end);

        style_parse(text, style, end - start);

        stylebuf->replace(start, end, style);
        editor->redisplay_range(start, end);
    }

    free(text);
    free(style);
}

void style_unfinished_cb(int a, void* b)
{
    return;
}
#endif /// of SYNTAXHIGHLIGHT_SUPPORTS
