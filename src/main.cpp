#include "wMain.h"

#define DEF_APP_CLSNAME         "SHCCFLTK"

int main ( int argc, char** argv)
{
#ifdef DEBUG
    printf(" FLTK ABI version is %d\n", FLTK_ABI_VERSION );
#endif // DEBUG

    Fl::set_font( FL_FREE_FONT, "Tahoma" );
    Fl_Window::default_xclass( DEF_APP_CLSNAME );

    Fl_Tooltip::color( FL_DARK3 );
    Fl_Tooltip::textcolor( FL_WHITE );
    Fl_Tooltip::size( 11 );
    Fl_Tooltip::font( FL_FREE_FONT );

    // To drawing flat scheme, Need FLTK-1.3.4-1-ts !
    Fl::scheme( "flat" );

    wMain wM( argc, argv );

    return wM.Run();
}
