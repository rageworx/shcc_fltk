![shcc_fltk_03210.jpg](https://bitbucket.org/repo/9xqo6M/images/3058432875-shcc_fltk_03210.jpg)

# SHCC FLTK #

* Name of Syntax Highlight Code Converter
* It's now changed to support highlight.js[https://highlightjs.org/]
* An open source project helping easily copy and paste code to web or blog like TiStory.
* This is a part of open source project FLTK 1.3.4-1(-ts)

## Latest update ##

* Version 0.3.2.10
    - Compile need updated with latest version of FLTK-1.3.4-1-ts with flat scheme.
    - Upgrading syntax highlight for C/C++.

## Previous updates ##

* Version 0.3.1.8
    - Changed some designs
    - Now supporting last you did like windows position, size and language class.

* Version 0.3.0.3
    - reprogrammed from Delphi (it was not existed anymore)

## Requirements ##

* GCC/MinGW-W64 or gcc
* (if Windows)M-SYS 1.x 
* Open source libraries:
    1. FLTK ( http://fltk.org/ ), FLTK v1.3.4-1-ts ( https://github.com/rageworx/fltk-1.3.4-1-ts )
    1. minIni, Copyright (c) CompuPhase, 2008-2012
       http://www.apache.org/licenses/LICENSE-2.0

## License limits ##

* MIT License for this project.
* APACHE v2 for minini
* FLTK License (inc. GPLv2) for FLTK.